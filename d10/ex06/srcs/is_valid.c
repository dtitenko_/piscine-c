/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_valid.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 16:06:39 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/13 19:33:55 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "do_op.h"

int		is_valid_num(char *str)
{
	int i;

	i = ft_count_spaces(str);
	if (str[i] && (str[i] == '+' || str[i] == '-'))
		i++;
	if (ft_is_num(str[i]))
		return (1);
	return (0);
}

int		is_valid_operator(char *str)
{
	int		i;
	char	*opertators;

	opertators = "-+*/%";
	i = -1;
	if (ft_strlen(str) != 1)
		return (0);
	while (++i < ft_strlen(opertators))
		if (str[0] == opertators[i])
			return (1);
	return (0);
}
