/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_op.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 15:04:03 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/13 21:57:57 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "do_op.h"

void	print_err(char c)
{
	if (c == '/')
		ft_putstr("Stop : division by zero\n");
	else
		ft_putstr("Stop : modulo by zero\n");
}

int		do_op(int a, int b, char opc)
{
	t_op	ops[5];

	ops[0] = add;
	ops[1] = sub;
	ops[2] = mul;
	ops[3] = div;
	ops[4] = mod;
	if (opc == '+')
		return (ops[0](a, b));
	if (opc == '-')
		return (ops[1](a, b));
	if (opc == '*')
		return (ops[2](a, b));
	if (opc == '/')
		return (ops[3](a, b));
	return (ops[4](a, b));
}

int		main(int argc, char **argv)
{
	int		a;
	int		b;

	if (argc != 4)
		return (-1);
	if (!is_valid_operator(argv[2]))
	{
		ft_putchar('0');
		ft_putchar('\n');
		return (-1);
	}
	a = ft_atoi(argv[1]);
	b = ft_atoi(argv[3]);
	if ((argv[2][0] == '/' || argv[2][0] == '%') && b == 0)
		print_err(argv[2][0]);
	else
	{
		ft_putnbr(do_op(a, b, argv[2][0]));
		ft_putchar('\n');
	}
	return (0);
}
