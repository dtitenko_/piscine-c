/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 14:49:37 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/13 18:39:06 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <unistd.h>

size_t		ft_any(size_t **tab, size_t (*f)(size_t*));
size_t		ft_any_1d(size_t *tab, int (*f)(size_t));
size_t		ft_count_if(size_t **tab, size_t (*f)(size_t*));
void		ft_foreach(size_t *tab, int length, size_t (*f)(size_t));
int			ft_is_num(char c);
int			ft_count_spaces(char *str);
int			ft_atoi(char *str);
void		ft_putchar(char c);
void		ft_putnbr(int nb);
void		ft_putstr(char *str);
int			ft_strcmp(char *s1, char *s2);
int			ft_strlen(char *str);
void		ft_swap(int *a, int *b);
#endif
