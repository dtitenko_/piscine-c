/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_if_count.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/12 22:16:59 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/13 15:59:10 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_count_if(size_t **tab, size_t (*f)(size_t*))
{
	int i;
	int cnt;

	i = 0;
	cnt = 0;
	while (tab[i])
	{
		if (f(tab[i]))
			cnt++;
		i++;
	}
	return (cnt);
}
