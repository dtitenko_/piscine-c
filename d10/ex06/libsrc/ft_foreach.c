/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_foreach.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 15:51:55 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/13 18:38:58 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_foreach(size_t *tab, int length, size_t (*f)(size_t))
{
	int i;

	i = -1;
	while (++i < length)
		f(tab[i]);
}
