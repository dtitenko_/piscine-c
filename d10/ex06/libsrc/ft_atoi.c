/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 18:58:29 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/13 16:28:32 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_is_num(char c)
{
	if (c <= '9' && c >= '0')
		return (1);
	return (0);
}

int		ft_count_spaces(char *str)
{
	int i;

	i = 0;
	while ((str[i] >= '\t' && str[i] <= '\r') || str[i] == ' ')
		i++;
	return (i);
}

int		ft_atoi(char *str)
{
	int sign;
	int res;
	int i;

	sign = 1;
	i = ft_count_spaces(str);
	res = 0;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			sign = -1;
		i += 1;
	}
	while (str[i] != '\0')
	{
		if (!ft_is_num(str[i]))
			break ;
		res = res * 10 + (str[i] - '0');
		i++;
	}
	return (sign * res);
}
