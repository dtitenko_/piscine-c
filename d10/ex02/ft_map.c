/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/12 22:02:33 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/12 22:07:20 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_map(int *tab, int length, int (*f)(int))
{
	int *out;
	int i;

	out = (int *)malloc(sizeof(int) * length);
	i = -1;
	while (++i < length)
		out[i] = f(tab[i]);
	return (out);
}
