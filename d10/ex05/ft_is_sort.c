/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/12 22:45:32 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/12 23:01:01 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_sort(int *tab, int length, int (*f)(int, int))
{
	int i;
	int j;
	int flag;

	i = -1;
	if (length > 0)
		flag = f(tab[0], tab[length - 1]);
	while (++i < length - 1)
	{
		j = i;
		while (++j < length)
		{
			if (flag < 0 && f(tab[i], tab[j]) > 0)
				return (0);
			if (flag > 0 && f(tab[i], tab[j]) < 0)
				return (0);
			if (flag == 0 && f(tab[i], tab[j]) != 0)
				return (0);
		}
	}
	return (1);
}
