/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 17:47:59 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/15 17:50:35 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int		main(int argc, char *argv[])
{
	if (argc == 2)
	{
		if (ft_display_file(argv[1]) == -1)
		{
			ft_puterr("An read error occurred\n");
			return (-1);
		}
		return (0);
	}
	else if (argc > 2)
	{
		ft_putstr("Too many arguments.\n");
		return (-1);
	}
	else
	{
		ft_putstr("File name missing.\n");
		return (-1);
	}
}
