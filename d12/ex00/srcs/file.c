/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 17:29:16 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/15 17:47:40 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int		ft_display_file(char *filename)
{
	int		fd;
	char	buf[129];
	int		len;

	if ((fd = open(filename, O_RDONLY)) == -1)
		return (-1);
	while ((len = read(fd, buf, 128)))
	{
		buf[len] = '\0';
		ft_putstr(buf);
	}
	close(fd);
	return (0);
}
