/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 21:25:28 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/18 19:42:25 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

# include <unistd.h>

int		ft_read_stdin(void);
int		ft_cat(char *filename);
void	ft_printerr(char *name, char *file, int no);
void	ft_putchar(char c);
void	ft_putchar_err(char c);
void	ft_putstr(char *str);
int		ft_strcmp(char *s1, char *s2);
void	ft_puterr(char *msg);
#endif
