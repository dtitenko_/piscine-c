/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cat.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 18:01:39 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/15 20:09:24 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int		ft_cat(char *filename)
{
	int		fh;
	char	buf[29 * 1024 + 1];
	int		len;

	if ((fh = open(filename, O_RDONLY)) == -1)
		return (errno);
	while ((len = read(fh, buf, 29 * 1024)))
	{
		buf[len] = '\0';
		ft_putstr(buf);
	}
	close(fh);
	return (0);
}
