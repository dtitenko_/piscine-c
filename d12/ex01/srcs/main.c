/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 20:09:13 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/15 20:46:52 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int		main(int argc, char *argv[])
{
	int i;

	i = 1;
	if (argc >= 2)
	{
		while (argv[i])
		{
			ft_printerr(argv[0], argv[i], ft_cat(argv[i]));
			i++;
		}
		return (0);
	}
	else
	{
		ft_read_stdin();
		return (0);
	}
}
