/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stdin.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 20:38:13 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/20 12:50:48 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int		ft_read_stdin(void)
{
	char	buf[29 * 1024 + 1];
	int		len;

	while ((len = read(0, buf, 29 * 1024)))
	{
		buf[len] = '\0';
		ft_putstr(buf);
	}
	return (0);
}
