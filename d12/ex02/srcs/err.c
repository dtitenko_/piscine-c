/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   err.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 18:00:56 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/15 20:10:18 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_tail.h"

void	ft_printerr(char *name, char *file, int no)
{
	if (no != 0)
	{
		ft_puterr(name);
		ft_puterr(": ");
		ft_puterr(file);
		ft_puterr(": ");
		if (no == ENOENT)
			ft_puterr("No such file or directory\n");
	}
}
