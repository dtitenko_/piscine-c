/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 20:08:40 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/15 21:36:12 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_tail.h"

int		main(int argc, char const **argv)
{
	int i;

	i = 1;
	if (argc >= 2)
	{
		while (argv[i])
		{
			ft_printerr(argv[0], argv[i], ft_tail(argv[i]));
			i++;
		}
		return (0);
	}
	else
	{
		ft_read_stdin();
		return (0);
	}
}
