/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_destroy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/09 13:06:00 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/09 13:06:02 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_ultimator.h"

void	ft_destroy(char ***factory)
{
	char **d;
	char *z;

	d = *factory;
	while (d)
	{
		z = *d;
		while (z)
			free(z++);
		free(z);
		d++;
		free(d - 1);
	}
	free(d);
	free(factory);
}
