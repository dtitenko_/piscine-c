/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_generic.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 17:51:51 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/08 18:06:34 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_generic(void)
{
	int		i;
	char	*tutu;

	i = -1;
	tutu = "Tu tu tu tu ; Tu tu tu tu\n";
	while (tutu[++i])
		write(1, &tutu[i], 1);
}
