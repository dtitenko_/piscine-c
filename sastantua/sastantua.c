/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sastantua.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/04 21:12:45 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/04 23:21:36 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	build_row(int width, int full_last_line)
{
	int j = 0;

	j = 0;
	while (j < (full_last_line - width )/ 2)
	{
		write(1, " ", 1);
		j++;
	}
	write(1, "/", 1);
	j = 0;
	while (j < width - 2)
	{
		write(1, "*", 1);
		j++;
	}
	write(1, "\\\n", 2);
}

int		count_last_row_flour(int start_line, int rows)
{
	return (start_line + (rows - 1) * 2);
}

int		count_last_row(int size)
{
	int i;
	int start_line;
	int rows;
	int more;
	int last_line;

	more = 4;
	start_line = 3;
	rows = 3;
	i = 0;
	while (i < size)
	{
		last_line = count_last_row_flour(start_line, rows + i);
		if (i % 2 != 1)
			more += 2;
		start_line = last_line + more;
		i++;
	}
	return (last_line);
}

void	build_floure(int start_line, int rows, int full_last_line)
{
	int i;

	i = 0;
	while (i < rows)
	{
		build_row(start_line + 2 * i, full_last_line);
		i++;
	}
}

void	build_last_floure(int start_line, int rows, int full_lastline, int size)
{
	int door_size;
	int i;
	int j;
	int width;

	door_size = size;
	i = 0;
	j = 0;
	if (size % 2 == 0)
		door_size = size - 1;
	while (i < rows - door_size)
	{
		build_row(start_line + 2 * i, full_lastline);
		i++;
	}
	i = 0;
	while(i < door_size)
	{
		j = 0;
		while (j < (full_lastline - (start_line + 2 * i)) / 2)
		{
			write(1, " ", 1);
			j++;
		}
		write(1, "/", 1);
		j = 0;
		while (j < (start_line + 2 * i - door_size) / 2)
		{
			write(1, "*", 1);
			j++;
		}
		while (j < (start_line + door_size + 2 * i) / 2)
		{
			write(1, "|", 1);
			j++;
		}
		while (j < (start_line +2 * i))
		{
			write(1, "*", 1);
		}
		write(1, "\\\n", 1);

	}
}

void	build_all_flours(size)
{
	int i;
	int start_line;
	int rows;
	int more;
	int last_line;
	int full_last_line;

	more = 4;
	start_line = 3;
	rows = 3;
	i = 0;
	full_last_line = count_last_row(size);
	while (i++ < size - 1)
	{
		build_floure(start_line, rows + i - 1, full_last_line);
		last_line = count_last_row_flour(start_line, rows + i);
		if (i % 2 != 1)
			more += 2;
		start_line = last_line + more;
	}
	build_last_floure(start_line, rows + i, full_last_line, size);
}
