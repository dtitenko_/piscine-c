/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   match.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/11 15:36:43 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/11 16:05:35 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		match(char *s1, char *s2)
{
	if (*s1 && *s2 == '*')
		return (match(s1 + 1, s2) || match(s1, s2 + 1));
	if (!*s1 && *s2 == '*')
		return (match(s1, s2 + 1));
	if (*s1 && *s2 && *s1 == *s2)
		return (match(s1 + 1, s2 + 1));
	if (!*s1 && !*s2)
		return (1);
	return (0);
}
