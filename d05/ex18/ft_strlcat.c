/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 19:05:50 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/06 22:39:32 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int					ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i++])
		;
	return (i);
}

unsigned int		ft_strlcat(char *dst, char *src, unsigned int nb)
{
	char			*d;
	char			*s;
	unsigned int	n;
	unsigned int	dlen;

	d = dst;
	s = src;
	n = nb;
	while (*d != '\0' && n-- != 0)
		d++;
	dlen = d - dst;
	n = nb - dlen;
	if (n == 0)
		return (dlen + ft_strlen(s) - 1);
	while (*s != '\0')
	{
		if (n != 1)
		{
			*d++ = *s;
			n--;
		}
		s++;
	}
	*d = '\0';
	return (dlen + (s - src));
}
