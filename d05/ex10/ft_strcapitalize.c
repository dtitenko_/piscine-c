/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 20:46:24 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/06 21:28:22 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_num(char c)
{
	if (c <= '9' && c >= '0')
		return (1);
	return (0);
}

int		ft_is_alpha(char c)
{
	if ((c <= 'z' && c >= 'a') || (c >= 'A' && c <= 'Z'))
		return (1);
	return (0);
}

int		ft_is_uppercase(char c)
{
	if (c >= 'A' && c <= 'Z')
		return (1);
	return (0);
}

char	*ft_strcapitalize(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (ft_is_alpha(str[i]))
		{
			if (str[i - 1])
			{
				if (!ft_is_alpha(str[i - 1]) && !ft_is_num(str[i - 1]))
					if (!ft_is_uppercase(str[i]))
						str[i] -= 'a' - 'A';
				if (ft_is_num(str[i - 1]) || ft_is_alpha(str[i - 1]))
					if (ft_is_uppercase(str[i]))
						str[i] += 'a' - 'A';
			}
			else
			{
				if (ft_is_alpha(str[i]))
					str[i] -= 'a' - 'A';
			}
		}
		i++;
	}
	return (str);
}
