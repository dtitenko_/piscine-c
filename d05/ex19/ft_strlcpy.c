/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 21:53:16 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/06 22:32:55 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int siz)
{
	char			*d;
	char			*s;
	unsigned int	n;

	d = dest;
	s = src;
	n = siz;
	if (n != 0 && --n != 0)
	{
		n++;
		while (--n != 0)
		{
			if ((*d++ = *s++) == 0)
				break ;
		}
	}
	if (n == 0)
	{
		if (siz != 0)
			*d = '\0';
		while (*s++)
			;
	}
	return (s - src - 1);
}
