/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 17:51:47 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/06 18:28:15 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncat(char *dest, char *src, int nb)
{
	int		j;
	char	*rdest;

	rdest = dest;
	j = 1;
	while (*dest)
		dest++;
	while ((*dest++ = *src++) && j++ < nb)
		;
	*(dest++) = '\0';
	return (rdest);
}
