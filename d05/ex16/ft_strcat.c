/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 15:10:03 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/06 18:23:44 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcat(char *dest, char *src)
{
	int		i;
	int		j;
	char	*rdest;

	rdest = dest;
	i = 0;
	j = 0;
	while (*dest)
		dest++;
	while ((*dest++ = *src++))
		;
	*(++dest) = '\0';
	return (rdest);
}
