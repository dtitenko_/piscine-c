/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/02 21:59:00 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/02 22:48:18 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str)
{
	int i;

	i = -1;
	while (str[++i] != '\0')
		;
	return (i);
}

char	*ft_strrev(char *str)
{
	char	tmp;
	int		len;
	int		k;

	len = ft_strlen(str);
	k = 0;
	tmp = '\0';
	while (k < len / 2)
	{
		tmp = str[len - k - 1];
		str[len - k - 1] = str[k];
		str[k] = tmp;
		k++;
	}
	return (str);
}
