/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_concat_params.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 14:13:30 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/08 21:50:51 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

char	*ft_strcat(char *dest, char *src)
{
	int		i;
	int		j;
	char	*rdest;

	rdest = dest;
	i = 0;
	j = 0;
	while (*dest)
		dest++;
	while ((*dest++ = *src++))
		;
	*(++dest) = '\0';
	return (rdest);
}

char	*ft_concat_params(int argc, char **argv)
{
	int		i;
	char	*tmp;
	int		len;

	len = argc + 1;
	i = 0;
	while (++i < argc)
		len += ft_strlen(argv[i]);
	tmp = (char *)malloc(len * sizeof(char));
	tmp[0] = '\0';
	i = 0;
	while (++i < argc)
	{
		tmp = ft_strcat(tmp, argv[i]);
		if (i != argc - 1)
			tmp = ft_strcat(tmp, "\n");
	}
	return (tmp);
}
