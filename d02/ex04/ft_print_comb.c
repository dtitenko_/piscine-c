/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 17:32:22 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/01 22:17:30 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c);

void	ft_print_comb(void)
{
	char d1;
	char d2;
	char d3;

	d1 = '0' - 1;
	while (d1 <= '7')
	{
		d2 = ++d1;
		while (d2 <= '8')
		{
			d3 = ++d2 + 1;
			while (d3 <= '9')
			{
				ft_putchar(d1);
				ft_putchar(d2);
				ft_putchar(d3);
				d3++;
				if (d1 != '7')
				{
					ft_putchar(' ');
					ft_putchar(',');
				}
			}
		}
	}
}
