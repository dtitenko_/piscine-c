/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/04 12:22:11 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/18 13:15:14 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define LEFT_T 'o';
#define RIGHT_T 'o';
#define LEFT_B 'o';
#define RIGHT_B 'o';
#define MID '-';
#define MID_L_R '|';

void	ft_putchar(char c);

void	ft_first_last_line(int x, int flag)
{
	int i;
	char left;
	char right;
	char mid;

	i = 0;
	mid = MID;
	if (flag == 0)
	{
		left = LEFT_T;
		right = RIGHT_T;
	}
	else
	{
		left = LEFT_B;
		right = RIGHT_B;
	}
	ft_putchar(left);
	while (x - 2 > i++)
	{
		ft_putchar(mid);
	}
	if (x > 1)
		ft_putchar(right);
	ft_putchar('\n');
}

void ft_mid_line(x)
{
	char mid_l_r;
	int i;

	i = 0;
	mid_l_r = MID_L_R;
	ft_putchar(mid_l_r);
	while (i < x - 2) {
		ft_putchar(' ');
		i++;
	}
	if (x > 1)
	{
		ft_putchar(mid_l_r);
	}
	ft_putchar('\n');
}

void ft_mid_lines(x, y)
{
	int i;

	i = 0;
	while (i < y-2)
	{
		mid_line(x);
		i++;
	}
}

void rush(x, y)
{
	if(x > 0 && y > 0)
	{
		first_last_line(x, 0);
		mid_lines(x, y);
		if (y > 1)
			first_last_line(x, 1);
	}
}
