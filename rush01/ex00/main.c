/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/11 18:49:19 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/11 22:09:26 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

#include <stdio.h>

int		main(int argc, char **argv)
{
	int **table;

	if (argc == 10)
	{
		if ((table = fill_table(argc, argv)) == NULL)
		{
			ft_putstr("Error\n");
			return (-1);
		}
		if (sudoku_solver(table, 0, 0))
		{
			print_table(table);
		}
		else
			ft_putstr("Error\n");
	}
	else
		ft_putstr("Error\n");
	return (0);
}
