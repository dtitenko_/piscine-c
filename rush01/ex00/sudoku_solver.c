/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku_solver.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/11 20:04:47 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/11 22:02:12 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

int		is_valid(int num, int **table, int row, int col)
{
	int i;
	int sector_col;
	int sector_row;

	i = -1;
	sector_row = 3 * (row / 3);
	sector_col = 3 * (col / 3);
	while (++i < 9)
	{
		if (table[i][col] == num)
			return (0);
		if (table[row][i] == num)
			return (0);
	}
	if (table[(row + 2) % 3 + sector_row][(col + 2) % 3 + sector_col] == num)
		return (0);
	if (table[(row + 4) % 3 + sector_row][(col + 2) % 3 + sector_col] == num)
		return (0);
	if (table[(row + 2) % 3 + sector_row][(col + 4) % 3 + sector_col] == num)
		return (0);
	if (table[(row + 4) % 3 + sector_row][(col + 4) % 3 + sector_col] == num)
		return (0);
	return (1);
}

int		iter_through(int **table, int col, int row)
{
	int next_num;

	next_num = 0;
	while (++next_num < 10)
	{
		if (is_valid(next_num, table, row, col))
		{
			table[row][col] = next_num;
			if (8 == col && sudoku_solver(table, row + 1, 0))
				return (1);
			else if (8 != col && sudoku_solver(table, row, col + 1))
				return (1);
		}
		table[row][col] = 0;
	}
	return (0);
}

int		sudoku_solver(int **table, int row, int col)
{
	if (9 == row)
		return (1);
	if (table[row][col])
	{
		if (8 == col && sudoku_solver(table, row + 1, 0))
			return (1);
		else if (8 != col && sudoku_solver(table, row, col + 1))
			return (1);
		return (0);
	}
	return (iter_through(table, col, row));
}
