/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_table.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/11 19:37:11 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/11 22:07:00 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

int		is_pos_digit(char c)
{
	if (c > '0' && c <= '9')
		return (1);
	return (0);
}

int		**fill_table(int ac, char **av)
{
	int		i;
	int		j;
	int		**table;

	i = -1;
	if ((table = malloc(sizeof(int *) * 9)) == NULL)
		return (NULL);
	while (++i < ac - 1)
	{
		if (ft_strlen(av[i + 1]) != 9)
			return (NULL);
		table[i] = malloc(sizeof(int) * 9);
		j = -1;
		while (++j < 9)
		{
			if (av[i + 1][j] == '.')
				table[i][j] = 0;
			else if (is_pos_digit(av[i + 1][j]))
				table[i][j] = av[i + 1][j] - '0';
			else
				return (NULL);
		}
	}
	return (table);
}

void	print_table(int **table)
{
	int i;
	int j;

	i = -1;
	while (++i < 9)
	{
		j = -1;
		while (++j < 8)
		{
			ft_putchar('0' + table[i][j]);
			ft_putchar(' ');
		}
		ft_putchar('0' + table[i][j]);
		ft_putchar('\n');
	}
}
