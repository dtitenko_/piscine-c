/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/20 12:09:08 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/21 20:34:17 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"
#include "ft.h"

int		main(int argc, char **argv)
{
	int		i;
	int		fd;

	i = 0;
	if (argc > 1)
	{
		while (++i < argc)
		{
			if ((fd = open(argv[i], O_RDONLY)) == -1)
			{
				ft_printerr(argv[0], argv[i], 2);
				continue ;
			}
			ft_printerr(argv[0], argv[i], solve(fd));
			close(fd);
		}
	}
	else
	{
		ft_printerr(argv[0], argv[i], solve(0));
	}
}
