/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/21 17:36:43 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/21 20:32:39 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"
#include "bsq.h"

int		**ft_mal(int **res, t_av input)
{
	int i;

	i = -1;
	res = (int **)malloc(sizeof(int *) * input.h);
	while (++i < input.h)
		res[i] = (int *)malloc(sizeof(int) * input.w);
	return (res);
}

int		ft_min(int a, int b, int c)
{
	int min;

	min = 0;
	min = a < b ? a : b;
	min = min < c ? min : c;
	return (min);
}

void	ft_f2_sqr(t_av input, int **res, int i, int j)
{
	if ((i == 0 || j == 0) && (input.tab[i][j] == input.charset[0]))
		res[i][j] = 1;
	else if (input.tab[i][j] == input.charset[1])
		res[i][j] = 0;
	else if (input.tab[i][j] == input.charset[0])
		res[i][j] = ft_min(res[i][j - 1], res[i - 1][j - 1],
							res[i - 1][j]) + 1;
}

int		ft_f_sqr(t_av input, int *i_p, int *j_p)
{
	int i;
	int j;
	int	**res;
	int size;

	size = 0;
	i = -1;
	res = 0;
	res = ft_mal(res, input);
	while (input.tab[++i])
	{
		j = -1;
		while (++j < input.w)
		{
			ft_f2_sqr(input, res, i, j);
			if (res[i][j] > size)
			{
				size = res[i][j];
				*i_p = i;
				*j_p = j;
			}
		}
	}
	return (size);
}

void	ft_result(t_av *input)
{
	int size;
	int i;
	int j;
	int *cnt;

	cnt = malloc(sizeof(int) * 3);
	i = 0;
	cnt[0] = -1;
	size = ft_f_sqr(*input, &i, &j);
	cnt[2] = j;
	while (++cnt[0] < size)
	{
		cnt[1] = -1;
		j = cnt[2];
		while (++cnt[1] < size)
		{
			input->tab[i][j] = input->charset[2];
			j--;
		}
		i--;
	}
}
