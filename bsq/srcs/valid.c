/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/20 13:57:34 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/21 13:56:25 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"
#include "ft.h"

int		isvalid_char(char c, char *charset)
{
	int i;

	i = -1;
	while (charset[++i])
		if (charset[i] == c)
			return (1);
	return (0);
}

int		isvalid_line(char *str, int len, char *charset)
{
	int i;

	i = -1;
	if (ft_strlen(str) != len)
		return (0);
	while (str[++i])
	{
		if (!isvalid_char(str[i], charset))
			return (0);
	}
	return (1);
}
