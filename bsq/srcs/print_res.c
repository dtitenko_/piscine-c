/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_res.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/21 18:26:15 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/21 20:36:24 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"
#include "ft.h"

void	print_res(t_av input)
{
	int i;

	i = -1;
	while (input.tab[++i])
	{
		write(1, input.tab[i], input.w);
		write(1, "\n", 1);
	}
}

int		solve(int fd)
{
	t_av	in;

	if ((in.tab = read_fd(fd, &in.charset, &in.w, &in.h)) == NULL)
		return (-1);
	ft_result(&in);
	print_res(in);
	return (0);
}
