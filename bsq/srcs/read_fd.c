/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_fd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/20 14:23:12 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/21 21:40:00 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"
#include "ft.h"

char	*ft_getstr(int fd, int n, char terminator)
{
	int		i;
	char	c;
	char	*str;

	i = -1;
	if ((str = malloc(sizeof(char) * n)) == NULL)
		return (NULL);
	while (read(fd, &c, 1) && c != terminator)
	{
		i++;
		if (n - i < 1)
		{
			n *= n / 2;
			if ((str = ft_realloc(str, n)) == NULL)
				return (NULL);
		}
		str[i] = c;
	}
	str[++i] = '\0';
	return (str);
}

int		get_legend(int fd, char **charset)
{
	int		len;
	int		nlines;
	char	*buf;
	int		i;
	char	*tmp;

	i = -1;
	buf = NULL;
	if (!(buf = ft_getstr(fd, 20, '\n')))
		return (-2);
	tmp = ft_strndup(buf, ft_strlen(buf) - 3);
	if (!ft_all_is_num(tmp))
		return (-2);
	nlines = ft_atoi(tmp);
	if ((*charset = malloc(4)) == NULL)
		return (-3);
	i = -1;
	len = ft_strlen(buf);
	ft_strncpy(*charset, (buf + len - 3), 3);
	if (charset[2] == charset[1] || charset[2] == charset[0])
		return (-2);
	if (charset[1] == charset[0])
		return (-2);
	return (nlines);
}

char	**read_fd(int fd, char **charset, int *len, int *nlines)
{
	int		i;
	char	*buf;
	char	**map;

	if (!(*nlines = get_legend(fd, charset)))
		return (NULL);
	i = -1;
	buf = NULL;
	if (!(buf = ft_getstr(fd, 20, -1)))
		return (NULL);
	*len = ft_strlen(buf);
	if (*(map = ft_split(buf, "\n")) == NULL)
		return (NULL);
	free(buf);
	*len = ft_strlen(map[0]);
	while (map[++i])
	{
		if (!isvalid_line(map[i], *len, *charset))
			return (NULL);
	}
	if (i != *nlines)
		return (NULL);
	return (map);
}
