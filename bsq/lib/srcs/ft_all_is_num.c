/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_all_is_num.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/21 20:49:22 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/21 21:33:00 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		ft_all_is_num(char *str)
{
	int i;

	i = -1;
	while (str[++i])
		if (!ft_is_num(str[i]))
			return (0);
	return (1);
}
