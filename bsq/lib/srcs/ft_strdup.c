/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 10:19:05 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/21 21:32:29 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

char	*ft_strdup(char *src)
{
	unsigned long	len;
	char			*copy;
	unsigned long	i;

	len = ft_strlen(src) + 1;
	if (!(copy = malloc(len)))
		return (NULL);
	i = 0;
	while (src[i])
	{
		copy[i] = src[i];
		i++;
	}
	copy[i] = '\0';
	return (copy);
}

char	*ft_strndup(char *src, unsigned int n)
{
	char			*copy;
	unsigned long	i;

	if (!(copy = malloc(n + 1)))
		return (NULL);
	i = 0;
	while (src[i] && i < n)
	{
		copy[i] = src[i];
		i++;
	}
	copy[i] = '\0';
	return (copy);
}
