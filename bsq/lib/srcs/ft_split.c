/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 17:43:34 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/20 18:29:52 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		instr(char ch, char *str)
{
	while (*str != '\0' && *str != ch)
		++str;
	return (*str == ch);
}

int		count_size(char *str, char *charset)
{
	int		cnt;

	cnt = 1;
	while (*str != '\0')
	{
		if (instr(*str, charset) == 0 && instr(*(str + 1), charset) == 1)
			++cnt;
		++str;
	}
	return (cnt);
}

char	*rev_cutstr(char *str, int len)
{
	char	*res;

	res = malloc(sizeof(char) * (len + 1));
	*(res + len) = '\0';
	while (--len >= 0)
	{
		*(res + len) = *--str;
	}
	return (res);
}

char	**ft_split(char *str, char *charset)
{
	char	**res;
	int		lencnt;
	int		idx;
	int		cnt;

	lencnt = 0;
	idx = count_size(str, charset);
	res = malloc(sizeof(char *) * (idx + 1));
	cnt = 0;
	while (cnt < idx)
	{
		while (instr(*str, charset) == 1)
			++str;
		while (instr(*str, charset) == 0)
		{
			++str;
			++lencnt;
		}
		res[cnt] = rev_cutstr(str, lencnt);
		lencnt = 0;
		++cnt;
	}
	*(res + cnt - 1) = 0;
	return (res);
}
