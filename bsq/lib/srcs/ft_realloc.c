/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/20 13:02:52 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/20 20:35:14 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

char	*ft_realloc(char *ptr, int size)
{
	int		minsize;
	char	*newptr;

	newptr = malloc(size);
	if (newptr == NULL)
		return (NULL);
	if (ptr != NULL)
	{
		minsize = ft_strlen(ptr);
		if (size < minsize)
			minsize = size;
		ft_strncpy(newptr, ptr, minsize);
		free(ptr);
	}
	return (newptr);
}
