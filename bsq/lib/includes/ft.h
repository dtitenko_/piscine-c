/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/10 14:20:12 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/21 21:49:13 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_H
# define FT_H
# include <unistd.h>
# include <stdlib.h>

void	ft_putchar_err(char c);
void	ft_putchar(char c);
void	ft_putnbr(int nbr);
void	ft_swap(int *a, int *b);
void	ft_putstr(char *str);
void	ft_puterr(char *str);
int		ft_strlen(char *str);
int		ft_strcmp(char *s1, char *s2);
int		ft_atoi(char *str);
int		ft_count_spaces(char *str);
int		ft_is_num(char c);
char	*ft_realloc(char *arr, int size);
char	**ft_split(char *str, char *charset);
char	*ft_strdup(char *src);
char	*ft_strndup(char *src, unsigned int n);
int		ft_all_is_num(char *str);
char	*ft_strncpy(char *dest, char *src, unsigned int n);
#endif
