/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/20 18:22:35 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/21 21:49:25 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BSQ_H
# define BSQ_H
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>

typedef	struct	s_av
{
	char	**tab;
	int		w;
	int		h;
	char	*charset;
}				t_av;

char			*ft_getstr(int fd, int n, char terminator);
int				get_legend(int fd, char **charset);
char			**read_fd(int fd, char **charset, int *len, int *nlines);
int				isvalid_char(char c, char *charset);
int				isvalid_line(char *str, int len, char *charset);
void			ft_printerr(char *name, char *file, int no);
void			ft_result(t_av *input);
void			print_res(t_av input);
int				solve(int fd);
#endif
