/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fibonacci.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/03 20:12:41 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/03 20:22:52 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_fibonacci(int index)
{
	int n;

	if (index == 0)
		return (0);
	if (index < 0)
		return (-1);
	if (index <= 2)
		n = 1;
	else
	{
		n = ft_fibonacci(index - 1) + ft_fibonacci(index - 2);
	}
	return (n);
}
