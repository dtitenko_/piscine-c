/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/03 20:12:30 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/03 20:21:55 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_power(int nb, int power)
{
	int out;
	int i;

	out = 1;
	i = 1;
	if (power < 0)
		return (0);
	while (i <= power)
	{
		out *= nb;
		i++;
	}
	return (out);
}
