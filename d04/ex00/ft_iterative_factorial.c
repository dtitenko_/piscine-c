/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/03 20:04:24 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/03 20:19:38 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_factorial(int nb)
{
	int n;

	n = 1;
	if ((nb < 0) || (nb > 12))
		return (0);
	if (nb == 1)
		return (1);
	while (nb > 1)
	{
		n *= nb;
		nb--;
	}
	return (n);
}
