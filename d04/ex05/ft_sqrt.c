/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/03 20:12:44 by dtitenko          #+#    #+#             */
/*   Updated: 2016/09/03 20:23:37 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_sqrt(int nb)
{
	int	sqrt;

	sqrt = 0;
	if (nb > 0)
	{
		while (sqrt * sqrt < nb)
		{
			sqrt++;
		}
		if (sqrt * sqrt == nb)
			return (sqrt);
		else
			return (0);
	}
	else
		return (0);
}
